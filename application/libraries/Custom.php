<?php
class Custom
{
  protected $ci;
  public function __construct()
  {
    $this->ci = &get_instance();
  }

  public function template($path ,$dat = null)
  {
    $data['head'] = $this->ci->load->view('Layouts/head',$dat,TRUE);
    $data['menu'] = $this->ci->load->view('Layouts/leftPanel',$dat,TRUE);
    $data['content']=  $this->ci->load->view($path,$dat,TRUE);
    $data['header'] = $this->ci->load->view('Layouts/header',$dat,TRUE);
    $data['footer'] = $this->ci->load->view('Layouts/footer',$dat,TRUE);

    $this->ci->load->view('Layouts/master',$data,FALSE);
  }


}

 ?>
